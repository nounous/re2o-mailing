from configparser import ConfigParser
import socket
import datetime

from re2oapi import Re2oAPIClient

config = ConfigParser()
config.read('config.ini')

api_hostname = config.get('Re2o', 'hostname')
api_password = config.get('Re2o', 'password')
api_username = config.get('Re2o', 'username')


fallback = config.getboolean('DEFAULT', 'activate', fallback=False)


def write_generic_members_file(ml_name, members):
    if config.getboolean(ml_name, 'activate', fallback=fallback):
        members = "\n".join(m['email'] for m in members)
        filename = 'ml.{name}.list'.format(name=ml_name)
        with open(filename, 'w+') as f:
            f.write(members)


def write_standard_members_files(api_client):
    for ml in api_client.list_mailingstandard():
        write_generic_members_file(ml['name'], ml['members'])


def write_club_members_files(api_client):
    fallback = config.get('DEFAULT', 'activate', fallback=False)
    for ml in api_client.list_mailingclub():
        write_generic_members_file(ml['name'], ml['members'])
        write_generic_members_file(ml['name']+'-admin', ml['members'])


api_client = Re2oAPIClient(api_hostname, api_username, api_password)
client_hostname = socket.gethostname().split('.', 1)[0]


for service in api_client.list_servicesregen():
#    if service['hostname'] == client_hostname and \
#            service['service_name'] == 'dns' and \
#            service['need_regen']:
    write_standard_members_files(api_client)
    write_club_members_files(api_client)
#        api_client.patch(service['api_url'], data={'need_regen': False})
