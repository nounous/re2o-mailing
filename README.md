## Re2o - Mailing

This service uses Re2o API to generate mailing member files.


## Requirements

* python3
* requirements in https://gitlab.federez.net/re2o/re2oapi
